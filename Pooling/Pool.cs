using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Object = UnityEngine.Object;

namespace Toolkit.Pooling
{
    public delegate TItem ItemCreator<TKey, out TItem>() where TItem : IPoolItem<TKey>;
    public delegate void ItemDestroyer<TKey, in TItem>(TItem item) where TItem : IPoolItem<TKey>;
    public delegate void ItemSpawner<in TKey, in TItem>(TKey key, TItem item) where TItem : IPoolItem<TKey>;
    public delegate void ItemDespawner<in TKey, in TItem>(TItem item) where TItem : IPoolItem<TKey>;
    
    public class Pool<TKey, TItem> where TItem : class, IPoolItem<TKey>
    {
        private readonly List<TItem> active = new List<TItem>();
        private readonly List<TItem> inactive = new List<TItem>();
        private readonly Dictionary<TKey, TItem> lookup = new Dictionary<TKey, TItem>();

        private readonly ItemCreator<TKey, TItem> creator;
        private readonly ItemDestroyer<TKey, TItem> destroyer;
        private readonly ItemSpawner<TKey, TItem> spawner;
        private readonly ItemDespawner<TKey, TItem> despawner;

        public IEnumerable<TKey> Ids => lookup.Keys;
        
        public IEnumerable<TItem> Active => active;

        public int TotalCount => active.Count + inactive.Count;

        public int ActiveCount => active.Count;
        
        public int InactiveCount => inactive.Count;
        
        public int LookupCount => lookup.Count;
        
        public bool Debug { get; set; }

        public Pool(
            ItemCreator<TKey, TItem> creator, 
            ItemDestroyer<TKey, TItem> destroyer,
            ItemSpawner<TKey, TItem> spawner, 
            ItemDespawner<TKey, TItem> despawner
            )
        {
            this.creator = creator ?? throw new ArgumentNullException(nameof(creator));
            this.destroyer = destroyer ?? throw new ArgumentNullException(nameof(destroyer));
            this.spawner = spawner ?? throw new ArgumentNullException(nameof(spawner));
            this.despawner = despawner ?? throw new ArgumentNullException(nameof(despawner));
        }

        public bool ContainsKey(TKey key) => lookup.ContainsKey(key);

        public bool TrySpawn(TKey key, out TItem item)
        {
            if (lookup.TryGetValue(key, out item) == true)
            {
                // already exists, not spawned 
                return false; 
            }

            if (inactive.Count > 0)
            {
                // get the next inactive item
                item = inactive[inactive.Count - 1];
                
                // remove it from the inactive list
                inactive.RemoveAt(inactive.Count - 1);
            }
            else
            {
                // create a new item
                item = creator(); 
            }

            item.Debug = Debug;
            
            // add to the active list 
            active.Add(item);
            
            // add to the lookup 
            lookup[key] = item;
            
            // call the spawner callback 
            spawner(key, item);

            return true; 
        }
        
        public bool TryDespawn(TItem item)
        {
            if (lookup.TryGetValue(item.Key, out TItem found) == false)
            {
                // does not exist in the lookup 
                return false; 
            }

            if (item != found)
            {
                // the item in the lookup does not match the item we are trying to despawn  
                return false; 
            }

            // remove from the lookup 
            lookup.Remove(item.Key); 
            
            // call the despawner
            despawner(item);
            
            return true; 
        }

        public bool TryGet(TKey key, out TItem item)
        {
            return lookup.TryGetValue(key, out item);
        }

        public void Maintain()
        {
            for (int i = active.Count - 1; i >= 0; i--)
            {
                TItem item = active[i];

                if (item.IsActive == true)
                {
                    continue;
                }

                // remove from the active pool
                active.RemoveAt(i); 
                
                // return to the inactive pool 
                inactive.Add(item);
            }
            
            for (int i = inactive.Count - 1; i >= 0; i--)
            {
                TItem item = inactive[i];

                if (item.IsDead == false)
                {
                    continue;
                }

                // remove from the inactive pool
                inactive.RemoveAt(i);

                // destroy the item 
                destroyer(item); 
            }
        }

        public void Clear()
        {
            foreach (TItem item in lookup.Values)
            {
                // call the despawner
                despawner(item);
            }
            
            // remove from the lookup 
            lookup.Clear();
        }

        public void Trim(int maxInactive)
        {
            for (int i = inactive.Count - 1; i >= maxInactive; i--)
            {
                TItem item = inactive[i];
                
                // remove from the inactive pool
                inactive.RemoveAt(i);

                // destroy the item 
                destroyer(item); 
            }
        }
    }
}