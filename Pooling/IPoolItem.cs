using System;

namespace Toolkit.Pooling
{
    public interface IPoolItem<TKey>
    {
        /// <summary>
        ///     Pool item key.
        /// </summary>
        TKey Key { get; }

        bool IsActive { get; }

        bool IsDead { get; }

        bool Debug { get; set; } 
    }
}