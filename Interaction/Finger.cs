﻿using System.Collections;
using TMPro;
using Toolkit;
using Toolkit.Pooling;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Toolkit.Interaction
{
    public class Finger : MonoBehaviour, IPoolItem<int>
    {
        [SerializeField] private TMP_Text DebugText;
        [SerializeField] private bool ShowDebug;
        
        [SerializeField] private Transform StartMarker;
        [SerializeField] private Transform EndMarker;
        [SerializeField] private Transform MoveMarker;

        [SerializeField] private UnityEvent OnStarted;
        [SerializeField] private UnityEvent OnDespawn;
        [SerializeField] private UnityEvent OnEnded;
        
        private FingerPhase phase;
        private Vector3 position;
        private Vector3 lastPosition;
        
        public FingerPhase Phase => phase; 

        /// <inheritdoc />
        public bool IsActive { get; private set; }

        /// <inheritdoc />
        public bool IsDead { get; private set; }

        /// <inheritdoc />
        public int Key { get; private set; }

        /// <inheritdoc />
        public bool Debug
        {
            get => ShowDebug;
            set => ShowDebug = value;
        }

        public float Speed => (lastPosition - position).magnitude * Time.deltaTime;

        public Vector3 Position => position;
        
        public Vector3 LastPosition => lastPosition;
        
        public void Despawn()
        {
            SetPhase(FingerPhase.Despawned);

            EndMarker.gameObject.SetActive(true);
            
            OnDespawn.Invoke();

            // StartCoroutine(WaitForEndAnimation());
        }

        public void Spawn(int key)
        {
            StartMarker.gameObject.SetActive(false);
            EndMarker.gameObject.SetActive(false);
            MoveMarker.gameObject.SetActive(false);

            if (DebugText != null)
            {
                DebugText.gameObject.SetActive(false);
            }

            Key = key;

            IsActive = true;

            SetPhase(FingerPhase.Spawned);
        }

        public void TouchBegin(Vector3 position)
        {
            MoveMarkers(position);

            lastPosition = position;
            StartMarker.position = position;

            StartMarker.gameObject.SetActive(true);
            MoveMarker.gameObject.SetActive(true);
            EndMarker.gameObject.SetActive(false);

#if UNITY_EDITOR || DEBUG
            if (DebugText != null && ShowDebug == true)
            {
                DebugText.gameObject.SetActive(true);
            }
#endif
            
            SetPhase(FingerPhase.Begin);
            
            OnStarted.Invoke();
        }

        public void TouchCanceled(Vector3 position)
        {
            MoveMarkers(position);

            lastPosition = position;

            EndMarker.gameObject.SetActive(true);

            SetPhase(FingerPhase.Canceled);
        }

        public void TouchEnded(Vector3 position)
        {
            MoveMarkers(position);
            
            lastPosition = position;

            EndMarker.gameObject.SetActive(true);

            SetPhase(FingerPhase.Ended);
        }

        public void TouchMoved(Vector3 position)
        {
            MoveMarkers(position);

            SetPhase(FingerPhase.Moved);
        }

        public void TouchStationary(Vector3 position)
        {
            MoveMarkers(position);

            SetPhase(FingerPhase.Stationary);
        }

        private void MoveMarkers(Vector3 position)
        {
            lastPosition = this.position;
            this.position = position;

            MoveMarker.position = position;
            EndMarker.position = position;
        }

        private void OnDestroy()
        {
            IsDead = true;
        }

        private void SetPhase(FingerPhase fingerPhase)
        {
            this.phase = fingerPhase;
            
#if UNITY_EDITOR || DEBUG
            if (DebugText != null && ShowDebug == true)
            {
                DebugText.transform.position = position; 
                DebugText.text = $"{Key} {fingerPhase}";
            }
#endif            
        }

        // private IEnumerator WaitForEndAnimation()
        // {
        //     yield return new WaitForSeconds(EndAnimationTime);
        
        public void EndAnimationFinished()
        {
            StartMarker.gameObject.SetActive(false);
            EndMarker.gameObject.SetActive(false);
            MoveMarker.gameObject.SetActive(false);

            if (DebugText != null)
            {
                DebugText.gameObject.SetActive(false);
            }
            
            OnEnded.Invoke();

            // mark as inactive 
            IsActive = false;
        }
    }
}