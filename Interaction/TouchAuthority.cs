﻿using System;
using System.Collections.Generic;
using TMPro;
using Toolkit.Interaction.Filters;
using Toolkit.Pooling;
using UnityEngine;

namespace Toolkit.Interaction
{
    public class TouchAuthority : MonoBehaviour
    {
        private static TouchAuthority authority;
        public static TouchAuthority Authority => authority; 
        
        private new Camera camera;

        // [SerializeField] private TMP_Text DebugText;
        // [SerializeField] private bool ShowDebug;

        // [SerializeField] private Finger FingerPrefab;

        private List<TouchFilter> Filters = new List<TouchFilter>();
        
        // [SerializeField] private Transform Container;

        // simulated touch mouse down  
        private bool isMouseDown;

        // private Pool<int, Finger> pool;
        private FingerPool pool;

        // simulated touch id
        private int simulatedTouchId;

        public void Register(FingerPool pool)
        {
            if (this.pool != null)
            {
                throw new Exception("Cannot register finger pool there is already a current finger pool.");
            }

            this.pool = pool; 
        }
        
        public void Unregister(FingerPool pool)
        {
            if (this.pool != pool)
            {
                throw new Exception("Finger pool is not the current finger pool.");
            }

            this.pool.Clear();

            this.pool = null; 
        }

        public void Register(TouchFilter filter)
        {
            if (Filters.Contains(filter))
            {
                throw new Exception("Interaction filter already present.");
            }
            
            Filters.Add(filter);
        }
        
        public void Unregister(TouchFilter filter)
        {
            if (Filters.Contains(filter) == false)
            {
                throw new Exception("Interaction filter not present.");
            }
            
            Filters.Remove(filter);
        }
        
        private void Awake()
        {
            if (authority != null)
            {
                throw new Exception("A touch authority is already present in the scene.");
            }

            authority = this;

            camera = Camera.main;

            // pool = new Pool<int, Finger>(Creator, Destroyer, Spawner, Despawner);
            //
            // pool.Debug = ShowDebug; 
        }

        private void HandleTouches(float worldZ)
        {
            foreach (Touch touch in Input.touches)
            {
                // try and get the finger object for this touch id, if none exists then spawn one 
                bool spawned = pool.TrySpawn(touch.fingerId, out Finger finger);

                Vector3 worldPosition = camera.ScreenToWorldPoint(touch.position);

                worldPosition.z = worldZ;

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        finger.TouchBegin(worldPosition);
                        break;
                    case TouchPhase.Moved:
                        finger.TouchMoved(worldPosition);
                        break;
                    case TouchPhase.Stationary:
                        finger.TouchStationary(worldPosition);
                        break;
                    case TouchPhase.Ended:
                        finger.TouchEnded(worldPosition);
                        break;
                    case TouchPhase.Canceled:
                        finger.TouchCanceled(worldPosition);
                        break;
                    default: throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void SimulateTouches(float worldZ)
        {
            bool mouseDown = Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2);

            bool mouseDownInPreviousFrame = isMouseDown;

            isMouseDown = mouseDown;

            Vector3 worldPosition = camera.ScreenToWorldPoint(Input.mousePosition);

            worldPosition.z = worldZ;

            if (mouseDown && mouseDownInPreviousFrame == false)
            {
                simulatedTouchId = (simulatedTouchId + 1) % 256;

                pool.TrySpawn(simulatedTouchId, out Finger finger);

                finger.TouchBegin(worldPosition);
            }
            else if (mouseDown && mouseDownInPreviousFrame)
            {
                pool.TrySpawn(simulatedTouchId, out Finger finger);

                finger.TouchMoved(worldPosition);
            }
            else if (mouseDownInPreviousFrame)
            {
                pool.TrySpawn(simulatedTouchId, out Finger finger);

                finger.TouchEnded(worldPosition);
            }
        }
        
        private void Update()
        {
            if (pool == null)
            {
                return;
            }

            float worldZ = transform.position.z;

            if (Input.touchSupported)
            {
                HandleTouches(worldZ);
            }
            else if (Input.mousePresent)
            {
                SimulateTouches(worldZ);
            }
            else
            {
                // if there is no mouse or touch then exit 
                Application.Quit();
            }
            
            foreach (TouchFilter filter in Filters)
            {
                filter.Process(pool.Active); 
            }

            foreach (Finger finger in pool.Active)
            {
                switch (finger.Phase)
                {
                    case FingerPhase.Spawned: break;
                    case FingerPhase.Despawned: break;
                    case FingerPhase.Begin: break;
                    case FingerPhase.Moved: break;
                    case FingerPhase.Stationary: break;
                    case FingerPhase.Ended: 
                    case FingerPhase.Canceled:
                        pool.TryDespawn(finger); 
                        break;
                    default: throw new ArgumentOutOfRangeException();
                }
            }
            
            // run maintenance on the pool 
            pool.Maintain();
        }

        // #region Pool Managment Delegates
        //
        // private Finger Creator()
        // {
        //     Transform container = Container;
        //     
        //     if (container == null)
        //     {
        //         container = transform; 
        //     }
        //
        //     return Instantiate(FingerPrefab, container);
        // }
        //
        // private void Despawner(Finger item)
        // {
        //     item.Despawn();
        // }
        //
        // private void Destroyer(Finger item)
        // {
        //     Destroy(item);
        // }
        //
        // private void Spawner(int key, Finger item)
        // {
        //     item.Spawn(key);
        // }
        //
        // #endregion
    }
}