﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Toolkit.Interaction.Filters
{
    public class DragDropable : MonoBehaviour
    {
        public event Action<DragDropable> Dropped;
        public event Action<DragDropable> PickedUp;
        public event Action<DragDropable> Dragging;

        private int lastCount = 0;
        
        private Dictionary<int, Vector3> touches = new Dictionary<int, Vector3>();
        
        private Vector3 accumulatedPosition = Vector3.zero; 
        
        public void AddTouch(int fingerKey, Vector3 hitPoint)
        {
            touches[fingerKey] = hitPoint;
        }

        public void RemoveTouch(int fingerKey)
        {
            touches.Remove(fingerKey);
        }

        public int TouchCount => touches.Count;

        public void AccumulatePosition(int fingerKey, Vector3 fingerPosition)
        {
            if (touches.TryGetValue(fingerKey, out Vector3 offset) == false)
            {
                return;
            }

            accumulatedPosition += fingerPosition - offset;
        }

        private void LateUpdate()
        {
            if (TouchCount != 0)
            {
                transform.position = accumulatedPosition / TouchCount;
            }

            accumulatedPosition = Vector3.zero;

            if (TouchCount != 0 && lastCount == 0)
            {
                PickedUp?.Invoke(this);
            }
            else if (TouchCount == 0 && lastCount != 0)
            {
                Dropped?.Invoke(this);
            }
            else if (TouchCount != 0 && lastCount != 0)
            {
                Dragging?.Invoke(this);
            }

            lastCount = TouchCount;
        }
    }
}
