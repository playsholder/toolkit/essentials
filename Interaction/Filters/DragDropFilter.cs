using System;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit.Interaction.Filters
{
    public class DragDropFilter : TouchFilter
    {
        private Camera mainCamera;

        [SerializeField] private LayerMask dragDropLayerMask;

        [SerializeField] private bool pickupMode = true;
        
        private Dictionary<int, DragDropable> activeDragDropables = new Dictionary<int, DragDropable>();
        
        public bool AllowDragging { get; set; }

        private void Awake()
        {
            mainCamera = Camera.main;
        }

        public void Clear()
        {
            foreach (KeyValuePair<int, DragDropable> pair in activeDragDropables)
            {
                pair.Value.RemoveTouch(pair.Key);
            }

            activeDragDropables.Clear();
        }

        /// <inheritdoc />
        public override void Process(IEnumerable<Finger> active)
        {
            if (AllowDragging == false)
            {
                return;
            }

            foreach (Finger finger in active)
            {
                switch (finger.Phase)
                {
                    case FingerPhase.Begin:
                    {
                        GetDragableViaRaycast(finger);

                        // dragDropLayerMask

                        // Ray ray = new Ray(finger.Position, mainCamera.transform.forward); // mainCamera.ScreenPointToRay2D(finger.Position));mainCamera.ScreenPointToRay2D(finger.Position);
                        //
                        // RaycastHit hit;
                        //
                        // if (Physics.Raycast(ray, out hit))
                        // {    
                        //     Debug.Log(hit);
                        // }
                        break;
                    }

                    case FingerPhase.Moved:
                    case FingerPhase.Stationary:
                    {
                        if (activeDragDropables.TryGetValue(finger.Key, out DragDropable dragDropable) == false)
                        {
                            if (pickupMode == true)
                            {
                                GetDragableViaRaycast(finger);
                            }

                            continue;
                        }

                        dragDropable.AccumulatePosition(finger.Key, finger.Position); 
                        
                        break;
                    }
                    
                    case FingerPhase.Canceled:
                    case FingerPhase.Ended:
                    {
                        if (activeDragDropables.TryGetValue(finger.Key, out DragDropable dragDropable) == false)
                        {
                            continue;
                        }

                        activeDragDropables.Remove(finger.Key);
                        
                        dragDropable.RemoveTouch(finger.Key);
                        
                        break;
                    }
                }
            }
        }

        private void GetDragableViaRaycast(Finger finger)
        {
            RaycastHit2D hit = Physics2D.Raycast(finger.Position, mainCamera.transform.forward, 300, dragDropLayerMask);

            if (hit.collider == null)
            {
                return;
            }

            Debug.Log(hit);

            DragDropable dragDropable = hit.transform.gameObject.GetComponent<DragDropable>();

            if (dragDropable == null)
            {
                return;
            }

            Vector3 localPoint = hit.transform.InverseTransformPoint(hit.point);

            activeDragDropables[finger.Key] = dragDropable;

            dragDropable.AddTouch(finger.Key, localPoint);

            dragDropable.AccumulatePosition(finger.Key, finger.Position);
        }
    }
}