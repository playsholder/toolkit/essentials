using System;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit.Interaction.Filters
{
    public abstract class TouchFilter : MonoBehaviour
    {
        private void OnEnable() => TouchAuthority.Authority.Register(this);
        private void OnDisable() => TouchAuthority.Authority.Unregister(this);

        public abstract void Process(IEnumerable<Finger> active);
    }
}