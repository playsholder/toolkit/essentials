using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

namespace Toolkit.Interaction.Filters
{
    public class DurationFilter : TouchFilter
    {
        private static readonly int ID_Value = Animator.StringToHash("Value");
        
        [SerializeField] private Animator DurationAnimation;
        
        [SerializeField] private float DurationSpeed;
        
        private float durationValue = 0;

        /// <inheritdoc />
        public override void Process(IEnumerable<Finger> active)
        {
            int touchCount = 0; 
            
            foreach (Finger finger in active)
            {
                switch (finger.Phase)
                {
                    case FingerPhase.Spawned: break;
                    case FingerPhase.Despawned: break;
                    case FingerPhase.Begin: 
                    case FingerPhase.Moved: 
                    case FingerPhase.Stationary:
                        touchCount++;
                        break;
                    case FingerPhase.Ended: break;
                    case FingerPhase.Canceled: break;
                    default: throw new ArgumentOutOfRangeException();
                }
            }

            if (touchCount == 0)
            {
                return; 
            }

            durationValue = (durationValue + DurationSpeed * Time.deltaTime) % 1.0f; 
                
            DurationAnimation.SetFloat(ID_Value, durationValue);
        }
    }
}