﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit.Interaction.Filters
{
    public class TouchSoundEventFilter : TouchFilter
    {
        [SerializeField] private AudioSource[] Sources = new AudioSource[0];

        [SerializeField] private FingerPhase Phase = FingerPhase.Begin;  
        
        private int index;

        public void PlayNext()
        {
            Sources[index].Play();
            
            index = (index + 1) % Sources.Length;
        }

        /// <inheritdoc />
        public override void Process(IEnumerable<Finger> active)
        {
            foreach (Finger finger in active)
            {
                if (finger.Phase != Phase)
                {
                    continue; 
                }
                
                PlayNext();

                return; 
            }
        }
    }
}