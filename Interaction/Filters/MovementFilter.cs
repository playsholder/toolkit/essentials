using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Toolkit.Interaction.Filters
{
    public class MovementFilter : TouchFilter
    {
        private static readonly int ID_Speed = Animator.StringToHash("Speed");
        
        [FormerlySerializedAs("MovementSound")] public Animator Animator; 
        [FormerlySerializedAs("MovementEffect")] public float Amount;
        [FormerlySerializedAs("MovementLerp")] public float LerpTime;

        private float currentSpeed = 0;
        
        /// <inheritdoc />
        public override void Process(IEnumerable<Finger> active)
        {
            float speed = 0; 
            
            foreach (Finger finger in active)
            {
                speed = Math.Max(speed, finger.Speed); 
            }
            
            currentSpeed = Mathf.Lerp(currentSpeed, speed, LerpTime * Time.deltaTime); 
            
            Animator.SetFloat(ID_Speed, Mathf.Clamp01(currentSpeed * Amount));
        }
    }
}