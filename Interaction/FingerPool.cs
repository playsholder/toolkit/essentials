using System;
using System.Collections.Generic;
using TMPro;
using Toolkit.Pooling;
using UnityEngine;
using UnityEngine.Serialization;

namespace Toolkit.Interaction
{
    public class FingerPool : MonoBehaviour
    {
        [NonSerialized] public readonly Pool<int, Finger> pool;
        [FormerlySerializedAs("FingerPrefab"),SerializeField] private Finger prefab;

        public IEnumerable<Finger> Active => pool.Active;

        public IEnumerable<int> Ids => pool.Ids;

        public FingerPool()
        {
            pool = new Pool<int, Finger>(Creator, Destroyer, Spawner, Despawner);
        }

        public void Clear() { }

        public void Maintain() => pool.Maintain();

        public bool TryDespawn(Finger finger) => pool.TryDespawn(finger);

        public bool TrySpawn(int touchFingerId, out Finger finger) => pool.TrySpawn(touchFingerId, out finger);

        private void OnDisable() => TouchAuthority.Authority.Unregister(this);

        private void OnEnable() => TouchAuthority.Authority.Register(this);

        #region Pool Managment Delegates

        private Finger Creator() => Instantiate(prefab, transform);

        private void Despawner(Finger item) => item.Despawn();

        private void Destroyer(Finger item) => Destroy(item.gameObject);

        private void Spawner(int key, Finger item) => item.Spawn(key);

        #endregion
    }
}