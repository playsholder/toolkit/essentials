namespace Toolkit.Interaction
{
    public enum FingerPhase
    {
        Spawned,
        Despawned,
        Begin,
        Moved,
        Stationary,
        Ended,
        Canceled
    }
}